
Feature: Plan de pruebas

  Scenario: Actualizar el nombre de la mascota y su estado
    Given url 'https://petstore.swagger.io/v2/pet'
    When request {"id": 1991121,"category": {"id": 0,"name": "Bulldog"},"name": "Mony pop","photoUrls": ["string"],"tags": [{"id": 0,"name": "Perro"}],"status": "sold"}
    And method PUT
    Then status 200